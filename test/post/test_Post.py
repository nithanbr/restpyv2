from test.test_base import root_dir, get_test_data, rest_client2
from src.utility.common_utility import log, read_json_file
import pytest


@pytest.mark.parametrize("name,job", get_test_data(root_dir + "\\resource\\data\\post-test.csv"))
def test_post(name, job):
    client = rest_client2

    data = read_json_file(root_dir + "\\resource\\request\\postRequest.json")
    data['name'] = name
    data['job'] = job

    response = client.execute_post(data)

    assert response.status_code == 201

    json_response = response.json()
    assert json_response['name'] != ""
