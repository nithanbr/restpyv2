import test.test_base as base
from src.core.api_client import RestClientOne, RestClientTwo
from src.utility.common_utility import log
import inspect

def pytest_sessionstart():
    # Below will help to configure multiple clients for multiple apis
    base.rest_client1 = RestClientOne(base.root_dir + "\\resource\\config\\api-config.ini")
    base.rest_client2 = RestClientTwo(base.root_dir + "\\resource\\config\\api-config.ini")
    log("***  Before Suite set up is done  *** \n")


def pytest_sessionfinish():
    log("***  After Suite Task is Done ***\n")


def pytest_runtest_logstart(nodeid):
    log(f'** Executing test : {nodeid} \n')


def pytest_runtest_logfinish(nodeid):
    log(f'** Execution completed for : {nodeid} \n')


def pytest_configure(config):
    base.root_dir = config.rootdir


def pytest_html_report_title(report):
    report.title = "My Test Report!"


def pytest_runtest_call(__multicall__, item):
    try:
        log(f'** Executing test : {item} \n')
        __multicall__.execute()
        log(f'** Execution completed for : {item} \n')
    except Exception as exp:
        log(f'Execution Failed for {item} : {exp}')
        raise
