from src.utility import common_utility

rest_client1 = None
rest_client2 = None
root_dir = None


def get_test_data(file_path):
    print(file_path)
    data = common_utility.read_csv_file(file_path)
    for row in data:
        if len(row) != 1:
            yield row
        else:
            for val in row:
                yield val
