from test.test_base import root_dir, get_test_data, rest_client1
import pytest


@pytest.mark.parametrize("user_id,", get_test_data(root_dir + "\\resource\\data\\get-test.csv"))
def test_get(user_id):
    client = rest_client1
    response = client.execute_get(user_id)

    assert response.status_code == 200

    json_response = response.json()
    assert json_response['data']['first_name'] != ""
