from src.utility.common_utility import read_json_file, read_config_file, log
import requests


class ApiClient:

    def __init__(self, config):
        self.reader = read_config_file(config)
        # self.headers = {"Content-Type": "application/json"}
        self.URL = self.reader.get('SampleApi', 'baseURL')

    def execute_post(self, request_body):
        try:
            url = self.URL + self.reader.get('SampleApi', 'Uri_CreateUser')
            log(f'*Request:{request_body}\n')
            response = requests.post(url, data=request_body)
            log(f'*Response:{response.json()}\n')
            return response
        except Exception as exp:
            log(f"** Post Call Failed - Exception- {exp}")
            raise

    def execute_get(self, params):
        try:
            url = self.URL + self.reader.get('SampleApi', 'Uri_getUserDetail') + "/" + params
            log(f'*Request Params:{params}\n')
            response = requests.get(url)
            log(f'*Response:{response.json()}\n')
            return response
        except Exception as exp:
            log(f"** Get Call Failed - Exception- {exp}")
            raise


class RestClientOne(ApiClient):
    def __init__(self, config):
        super().__init__(config)


class RestClientTwo(ApiClient):
    def __init__(self, config):
        super().__init__(config)
