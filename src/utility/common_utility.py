import json
import logging
import configparser
import csv


def read_json_file(file_path):
    with open(file_path) as file:
        json_request = json.load(file)
        log(json_request)
    return json_request


def read_config_file(file_path):
    reader = configparser.ConfigParser()
    reader.read(file_path)
    return reader


def read_csv_file(file_path):
    with open(file_path) as file:
        file = csv.reader(file, delimiter=",")
        line = 0
        data = []
        for row in file:
            if line == 0:
                line = line + 1
                continue
            else:
                data.append(row)
                line = line + 1
    return data


def log(message):
    logging.basicConfig(format='\n%(asctime)s - %(message)s', level=logging.INFO)
    logging.info(message)
