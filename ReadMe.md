Steps:

1. Install shining panda plugin in jenkins
2. configure python path in global tool configuration
3. In Build Configuration select Virtual Environment Builder
4. Select Python Version= Python , Nature = Shell
5. Add Below Commands
    pip install -r requirements.txt
    pytest --capture=sys --html=./report/report.html 
6. Execute the build and report will be available in report folder

useful links:

https://docs.pytest.org/en/stable/reference.html
https://comquent.de/en/how-to-build-a-python-project-in-jenkins/

create virtual environment without shiningpanda plugin:

https://softwaretester.info/jenkins-and-virtualenv/
http://iamnearlythere.com/jenkins-python-virtualenv/

# Setup a proper path, I call my virtualenv dir "venv" and
# I've got the virtualenv command installed in /usr/local/bin
PATH=$WORKSPACE/venv/bin:/usr/local/bin:$PATH
if [ ! -d "venv" ]; then
        virtualenv venv
fi
. venv/bin/activate
pip install -r requirements.txt --download-cache=/tmp/$JOB_NAME


----OR----
# set variable
ExampleENV="${WORKSPACE}"/.ExampleENV

# delete folder and content if exists
if [ -d "$ExampleENV" ]; then
	rm -fr "$ExampleENV"
fi

# create new virtualenv
virtualenv --no-site-packages "$ExampleENV"

# activate virtualenv
. "$ExampleENV"/bin/activate

# CODE FOR VIRTUALENV...